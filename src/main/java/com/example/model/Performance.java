package com.example.model;

import java.io.Serializable;

public class Performance implements Serializable {
    private String resultTime;
    private String granularityPeriod;
    private String objectName;
    private String cellId;
    private String callAttempts;

    public Performance() {
    }

    public Performance(String resultTime, String granularityPeriod, String objectName, String cellId, String callAttempts) {
        this.resultTime = resultTime;
        this.granularityPeriod = granularityPeriod;
        this.objectName = objectName;
        this.cellId = cellId;
        this.callAttempts = callAttempts;
    }

	public String getResultTime() {
		return resultTime;
	}

	public void setResultTime(String resultTime) {
		this.resultTime = resultTime;
	}

	public String getGranularityPeriod() {
		return granularityPeriod;
	}

	public void setGranularityPeriod(String granularityPeriod) {
		this.granularityPeriod = granularityPeriod;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public String getCallAttempts() {
		return callAttempts;
	}

	public void setCallAttempts(String callAttempts) {
		this.callAttempts = callAttempts;
	}


}
