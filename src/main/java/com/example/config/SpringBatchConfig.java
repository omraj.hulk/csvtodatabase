 package com.example.config;

import com.example.listener.JobListener;
import com.example.model.Performance;
import com.example.model.PerformanceDTO;
import com.example.processor.PerformanceProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;

    @Bean
    public FlatFileItemReader<Performance> reader() {
        FlatFileItemReader<Performance> reader = new FlatFileItemReader<Performance>();
        reader.setResource(new ClassPathResource("entity.csv"));

        reader.setLineMapper(new DefaultLineMapper<Performance>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "resultTime", "granularityPeriod","objectName","cellId","callAttempts" });
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper() {{
                setTargetType(Performance.class);
            }});
        }});
        return reader;
    }


    @Bean
    public PerformanceProcessor processor() {
        return new PerformanceProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<PerformanceDTO> writer() {
        JdbcBatchItemWriter<PerformanceDTO> writer = new JdbcBatchItemWriter<PerformanceDTO>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        writer.setSql("INSERT INTO performance (resultTime,granularityPeriod,objectName,cellId,callAttempts) " +
                "VALUES (:resultTime, :granularityPeriod,:objectName,:cellId,:callAttempts)");
        writer.setDataSource(dataSource);
        return writer;
    }

    @Bean
    public Job importUserJob(JobListener listener) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<Performance, PerformanceDTO> chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

}
