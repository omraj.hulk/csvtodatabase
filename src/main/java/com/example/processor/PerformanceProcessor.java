package com.example.processor;

import com.example.model.Performance;

import com.example.model.PerformanceDTO;
import org.springframework.batch.item.ItemProcessor;

public class PerformanceProcessor implements ItemProcessor<Performance, PerformanceDTO> {

    @Override
    public PerformanceDTO process(final Performance performance) throws Exception {
        System.out.println("Transforming Employee(s) to EmployeeDTO(s)..");
        final PerformanceDTO performanceDto = new PerformanceDTO(performance.getResultTime(), performance.getGranularityPeriod(),
        		performance.getObjectName(), performance.getCellId(),performance.getCallAttempts());

        return performanceDto;
    }

}
